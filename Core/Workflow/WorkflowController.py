from Core.Analyzer.AnalyzerController import AnalyzerController
from Core.ActionController import ActionController
from Core.Utils import Utils

import copy
class WorkflowController():
    def __init__(self, workflow):
        self.workflow = workflow
    def exec(self):
        resultList = {}
        for analyzer in copy.deepcopy(self.workflow['Analyzer']):
            analyzerParameter =  list(analyzer.values())[0]
            returnAnalayzer = AnalyzerController(analyzerParameter).exec()
            if not returnAnalayzer:
                return None
        for action in copy.deepcopy(self.workflow['Action']):
            actionName =  list(action.keys())[0]
            actionParameter = list(action.values())[0]
            Utils.replaceVariable(actionParameter,{**self.workflow,**resultList})
            resultList['_resultAction'] = ActionController(actionName,actionParameter).exec()
        return resultList
            