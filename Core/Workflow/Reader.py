import os
import yaml
from Core.Workflow.WorkflowManager import WorkflowManager

config = "Config"
workflowConfig = config+"/Workflow"
channelsConfig = config+"/Channels"
class Reader():
    def load():
        workflowList = {}
        workflowFiles = os.listdir(workflowConfig)
        for workflowFile in workflowFiles :
            workflowArray = yaml.full_load(open(workflowConfig+'/'+workflowFile,'r'))
            for workflowArrayStep in workflowArray:
                workflowArray[workflowArrayStep] = Reader.mergeConf(workflowArrayStep,workflowArray[workflowArrayStep])
            workflowFileName = workflowFile.split('.')[0]
            WorkflowManager.addWorkflow(workflowFileName, workflowArray)
        

    def mergeConf(step, array):
        for number, obj in enumerate(array):
            for key, value in obj.items():
                if '.' in key :
                    keyWord = key.split('.')
                    stepArray = yaml.full_load(open(channelsConfig+'/'+keyWord[0]+'/'+step+'/'+keyWord[1]+'.yml','r'))
                else :
                    stepArray = yaml.full_load(open(config+'/'+step+'/'+key+'.yml','r'))
                if value != None:
                    stepArray.update(value)
                array[number][key] = stepArray
        return array