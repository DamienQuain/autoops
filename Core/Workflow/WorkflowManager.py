import re
import base64
from Core.Workflow.WorkflowController import WorkflowController
class WorkflowManager():
    workflowList = {}
    workflowControllerList = {}
    def addWorkflow(workflowName,workflow):
        WorkflowManager.workflowList[workflowName] = workflow
        WorkflowManager.workflowControllerList[workflowName] = WorkflowController(workflow)

    def getWorflowTriggerParameters(channel):
        workflowTriggerFilterList = {}
        for workflowName, workflow in WorkflowManager.workflowList.items() :
            for trigger in workflow['Trigger']:
                    triggerName = list(trigger.keys())[0]
                    triggerParameter = list(trigger.values())[0]
                    if re.search('^'+channel+'\.',triggerName): # the trigger as only one name
                        if workflowName not in workflowTriggerFilterList:
                            workflowTriggerFilterList[workflowName] = []
                        workflowTriggerFilterList[workflowName].append(triggerParameter)
        return workflowTriggerFilterList
    
    def getWorkflow(workflowName):
        return WorkflowManager.workflowControllerList[workflowName]
