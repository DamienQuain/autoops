import importlib
from Core.ActionController import ActionController
from Core.Utils import Utils
class AnalyzerController():
    def __init__(self, analyzer):
        self.analyzer = analyzer
    def exec(self):
        resultList = {}
        for action in self.analyzer['Action']:
            actionName =  list(action.keys())[0]
            actionParameter = list(action.values())[0]
            Utils.replaceVariable(actionParameter,{**self.analyzer,**resultList})
            resultList['_resultAction'] = ActionController(actionName,actionParameter).exec()
        for check in self.analyzer['Check']:
            checkName = list(check.keys())[0]
            checkParameter = list(check.values())[0]
            Utils.replaceVariable(checkParameter,{**self.analyzer,**resultList})
            # dinamic import of the check
            module = importlib.import_module('Core.Analyzer.Check.'+checkName)
            checkClass = getattr(module,checkName)
            resultCheck = checkClass.exec(checkParameter)
            if not resultCheck :
                return False
        return True
