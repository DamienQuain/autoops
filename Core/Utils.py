import re
class Utils():
    def replaceVariable(parameter, workflowVariable):
        if isinstance(parameter, dict):
            for key,param in parameter.items() :
                parameter[key]=Utils.replaceVariable(param,workflowVariable)
        elif isinstance(parameter, str):
            if re.search('{{.*}}',parameter):
                variables = re.findall('\{\{(.*?)\}\}',parameter)
                for variable in variables:
                    parameter = re.sub('{{'+variable+'}}',workflowVariable[variable], parameter)
        return parameter
