import importlib
class ActionController():
    def __init__(self, name, parameter):
        self.name = name
        self.parameter = parameter
    def exec(self):
        nameSplited = self.name.split('.')
        try :
            module = importlib.import_module('Module.'+nameSplited[0]+'.Action.'+nameSplited[1])
            actionClass = getattr(module,nameSplited[1])
            return actionClass.exec(self.parameter)
        except Exception as e :
            print("impossible to find action "+ 'Module.'+nameSplited[0]+'.Action.'+nameSplited[1])