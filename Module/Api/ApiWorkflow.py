import falcon


class ApiWorkflow(object):
    def addWorkflow(self, method, workflowName, workflow):
        if method not in self.workflowList:
            self.workflowList[method] = {}
        self.workflowList[method][workflowName]= workflow
    def __init__(self):
        self.workflowList = {}
    def on_get(self, req, resp):
        """Handles GET requests"""
        if req.method in self.workflowList.keys() :
            response = None
            for workflow in self.workflowList[req.method].values():
                response = workflow.exec()
            if response != None:
                response = response['_resultAction']
                resp.status = eval('falcon.HTTP_'+str(response['status']))  # This is the default status
                resp.body = response['body']
        else :
            resp.status = falcon.HTTP_405

    def on_post(self, req, resp):
        """Handles POST requests"""
        if req.method in self.workflowList.keys() :
            response = None
            for workflow in self.workflowList[req.method].values():
                response = workflow.exec()
            if response != None:
                response = response['_resultAction']
                resp.status = eval('falcon.HTTP_'+str(response['status']))  # This is the default status
                resp.body = response['body']
        else :
            resp.status = falcon.HTTP_405

    def on_put(self, req, resp):
        """Handles PUT requests"""
        if req.method in self.workflowList.keys() :
            response = None
            for workflow in self.workflowList[req.method].values():
                response = workflow.exec()
            if response != None:
                response = response['_resultAction']
                resp.status = eval('falcon.HTTP_'+str(response['status']))  # This is the default status
                resp.body = response['body']
        else :
            resp.status = falcon.HTTP_405