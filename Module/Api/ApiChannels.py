import falcon
import re
from Module.Api.ApiWorkflow import ApiWorkflow

from Core.Workflow.WorkflowManager import WorkflowManager

class ApiChannels():
    def load():
        app = falcon.API()
        apilist = {}
        workflowParameters = WorkflowManager.getWorflowTriggerParameters('Api')
        for workflowName, trigger in workflowParameters.items() :
            for triggerParameters in trigger:
                if triggerParameters['route'] not in apilist:
                    apilist[triggerParameters['route']] =  ApiWorkflow()
                apilist[triggerParameters['route']].addWorkflow(
                    triggerParameters['method'],
                    workflowName,
                    WorkflowManager.getWorkflow(workflowName)
                )
        for apiRoute, api in apilist.items():
            app.add_route(apiRoute,api)
        return app